package com.fedka27.server.controller

import com.fedka27.server.dto.TestDto
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicLong

@RestController
class TestController {

    val counter = AtomicLong(1)

    @GetMapping("/test")
    fun getTestContent(
            @RequestParam("param", defaultValue = "all") param: String
    ): TestDto {
        return TestDto(counter.getAndSet(counter.get() * 2), "Param is $param")
    }

}