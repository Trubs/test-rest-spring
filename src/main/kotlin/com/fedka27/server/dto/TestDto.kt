package com.fedka27.server.dto

data class TestDto(
        val id: Long,
        val name: String
)