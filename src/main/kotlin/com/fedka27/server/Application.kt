package com.fedka27.server

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication


@SpringBootApplication
class Application {

    companion object {
        private val TAG = Application::class.simpleName

        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(Application::class.java, *args)

            println("$TAG: started")
        }

    }

}
